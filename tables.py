import numpy as np
import operator
import functools

def shift_row(row, rot):
    return np.concatenate((row[rot:], row[:rot]))

def shift_array(array, rot):
    for r,row in enumerate(array):
        array[r] = shift_row(row,rot)
    return array

students = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", 
        "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", ]

st_per_tbl = 3
tbls  = 8
num_students = st_per_tbl * tbls # this must be true, use "filler students if not)
students = students[:num_students]

tables = np.array(students).reshape((st_per_tbl,tbls))
print(tables)
rot_vec = [1, 3, 5] # all prime over GF(8) and mutually-prime

first_tables = tables.copy()
LCM = 7 #functools.reduce(operator.mul, rot_vec, 1) # Least Common Multiple (in this case)
for n in range(LCM+1): # +1 to show that we end up back where we started
    for row, rot in enumerate(rot_vec):
        tables[row] = shift_row(tables[row], rot)
    print(tables)
    # check against first
    for rot in range(tbls):
        if (tables == shift_array(first_tables,1)).all():
            print("after", n+1, "arrangements,", "it's the same, shifted by", rot)
            exit()


